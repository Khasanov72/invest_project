<?php

    return [
        'all' => [
            'login',
        ],
        'authorize' => [
            'profile',
            'logout',
        ],
        'guest' => [
            'login',
            'register',
            'recovery',
            'confirm',
            'reset',
        ],
        'admin' => [
            //
        ],
    ]
?>