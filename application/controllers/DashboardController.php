<?php

namespace application\controllers;

use application\core\Controller;
use application\lib\DB;

class DashboardController extends Controller
{
    public function investAction()
    {
        $vars = [
            'tariff' => $this->tariffs[$this->route['id']]
        ];
        // \debug($vars['tariff']);
        $this->view->render('Инвестировать', $vars);
        // debug($this->tariffs);
    }
    public function tariffsAction()
    {
        $this->view->render('Тарифы');
        // debug($this->tariffs);
    }
    public function historyAction()
    {
        $this->view->render('История');
        // debug($this->tariffs);
    }
    public function referralsAction()
    {
        $this->view->render('Рефералы');
        // debug($this->tariffs);
    }
}