<?php

namespace application\controllers;

use application\core\Controller;
use application\lib\DB;

class MainController extends Controller
{
    public function indexAction()
    {
        $vars = [
            'tariffs' => $this->tariffs,
        ];
        $this->view->render('Главная страница', $vars);
        // debug($this->tariffs);
    }
}