<?php

namespace application\core;

class View
{   

    public $path;
    public $layout = 'default';
    public $route;

    public function __construct($route)
    {
        $this->route = $route;
        $this->path = $route['controller'].'/'.$route['action'];
        // \debug($this->path);
    }

    public function render($title, $vars = []) {
        $view_path = 'application/views/'.$this->path.'.php';
        extract($vars);

        if (\file_exists($view_path)) {
            \ob_start();
            require 'application/views/'.$this->path.'.php';
            $content = ob_get_clean();
            require 'application/views/layouts/'.$this->layout.'.php';    
        } else {
            echo 'Вид не найден: '.$view_path;
        }
    }

    public static function errorCode($code)
    {
        http_response_code($code);
        $path = 'application/views/errors/'.$code.'.php';
        
        if (file_exists($path)) {
            require $path;
        }
        exit;
    }

    public function redirect(String $url)
    {
        header('Location: /'.$url);
        exit;
    }

    public function message(String $status, String $message)
    {
        exit(json_encode(['status' => $status, 'message' => $message]));
    }

    public function location(String $url)
    {
        exit(json_encode(['url' => $url]));
    }
}