<?php

namespace application\lib;

use PDO;

class DB 
{
    protected $db;

    public function __construct()
    {
       $config = require 'application/config/db.php';
       // debug($config);
       $this->db = new PDO('mysql:host='.$config['host'].';dbname='.$config['dbname'],$config['user'],$config['pass']);
    }

    public function query(String $sql = '', $params = []) {
        $stmt = $this->db->prepare($sql);
        if (!empty($params)) {
            foreach ($params as $key => $val) {
                if (is_int($val)) {
                    $type = PDO::PARAM_INT;
                } else {
                    $type = PDO::PARAM_STR;
                }
                // echo 'try to bind: '.$key.' '.$val.' type: '.$type.'</br>';
                $stmt->bindValue(':'.$key, $val, $type);
            }
        }
        // echo $stmt->queryString;
        $stmt->execute();
        return $stmt;
    }

    public function row(String $sql = '', $params = []) {
        $result = $this->query($sql, $params);
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

    public function column(String $sql = '', $params = [])
    {
        $result = $this->query($sql, $params);
        return $result->fetchColumn();
    }

    public function lastInsertId()
    {
        return $this->db->lastInsertId();
    }
}
