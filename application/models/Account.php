<?php

namespace application\models;

use application\core\Model;

class Account extends Model
{
    public function validate($input, $post)
    {   
        $rules = [
            'email' => [
                'pattern' => '#^([a-z0-9_.-]{1,20}+)@([a-z0-9_.-]+)\.([a-z\.]{2,10})$#',
                'message' => 'email-адрес указан не верно'
            ] ,
            'login' => [
                'pattern' => '#^[A-z0-9]{3,15}$#',
                'message' => 'Login указан неверно. Разрешены только латинские буквы и цифры от 3 до 15 символов',
            ] ,
            'ref' => [
                'pattern' => '#^[A-z0-9]{3,15}$#',
                'message' => 'Логин пригласившего указан неверно',
            ] ,
            'password' => [
                'pattern' => '#^[A-z0-9]{10,30}$#',
                'message' => 'Пароль указан неверно. Разрешены только латинские буквы и цифры от 10 до 20 символов'
            ] ,
            'wallet' => [
                'pattern' => '#^[U0-9]{3,15}$#',
                'message' => 'Кошелек Perfect Money указан неверно'
            ],
        ];

        foreach ((array)$input as $val) {
            if (!isset($post[$val]) or !preg_match($rules[$val]['pattern'], $post[$val])) {
                $this->error = $rules[$val]['message'];
                return false;
            } 
        }

        if (isset($post['ref'])) {
            if ($post['login'] == $post['ref']) {
                $this->error = 'Регистрация невозможна';
                return false;
            }
        }
        return true;
    }

    public function checkEmailExists($email)
    {
        return $this->db->column('SELECT id FROM accounts WHERE email = :email', ['email' => $email]);
    }
    
    public function checkloginExists($login)
    {       
        $params = [
            'login' => $login,
        ];

        $query = 'SELECT id FROM accounts WHERE login = :login';

        if ($this->db->column($query, $params)) {
            $this->error = 'Этот логин уже используется';
            return false;
        }
        return true;        
    }

    public function checkRefExists(String $login = '')
    {
        $params = [
            'login' => $login,
        ];

        return $this->db->column('SELECT id FROM accounts WHERE login = :login', $params);
    }

    public function register($post)
    {   
        $token = $this->createToken();
        if ($post['ref'] == 'none') {
            $ref = 0;
        } else {
            $ref = $this->checkRefExists($post['ref']);
            // \debug('ref for user '.$post['ref'].' is '.$ref);
            if ($ref == false) {
                $ref = 0;
            } 
        }
        
        $params = [
            'id' => NULL,
            'email' => $post['email'],
            'login' => $post['login'],
            'password' => password_hash($post['password'], PASSWORD_BCRYPT),
            'wallet' => $post['wallet'],
            'ref' => $ref,
            'token' => $token,
            'status' => 0,
        ];
        
        $this->db->query('INSERT INTO accounts VALUES (:id, :email, :login, :password, :wallet, :ref, :token, :status)', $params);
        mail($post['email'], 'Register', 'Confirm: http://study.local/account/confirm/'.$token);
    }

    public function activate(String $token = '')
    {
        $params = [
            'token' => $token,
        ];

        $this->db->query('UPDATE accounts SET status = 1, token = "" WHERE token = :token', $params);        
    }

    public function checkTokenExists(String $token = '')
    {
        $params = [
            'token' => $token
        ];

        $result = $this->db->column('SELECT id FROM accounts WHERE token = :token', $params);
        return $result;
    }

    public function createToken()
    {
        return substr(str_shuffle(str_repeat('0123456789abcdefghijklmnopqrstuvwxyz', 30)), 0, 30);
    }

    public function checkData(String $login = '', String $password = '')
    {
        $params = [
            'login' => $login
        ];

        $hash = $this->db->column('SELECT password FROM accounts WHERE login = :login', $params);
        if (!$hash or !password_verify($password, $hash)) {
            $this->error = "неверный пароль";
            return false;
        }
        return true;
    }

    public function checkStatus(String $type = '', String $data = '')
    {
        $params = [
            $type => $data
        ];

        $status = $this->db->column('SELECT status FROM accounts WHERE '.$type.' = :'.$type, $params);

        if ($status != 1) {
            $this->error = 'Аккаунт ожидает потверждения по E-mail';
            return false;
        }
        return true;
    }

    public function login(String $login = '')
    {
        $params = [
            'login' => $login,
        ];

        $data = $this->db->row('SELECT * FROM accounts WHERE login = :login', $params);
        $_SESSION['account'] = $data[0];
    }

    public function recovery(Array $post = [])
    {
        $token = $this->createToken();
        $params = [
            'email' => $post['email'],
            'token' => $token,
        ];
        $this->db->query('UPDATE accounts SET token = :token WHERE email = :email', $params);
        \mail($post['email'], 'Recovery', 'Confirm: http://study.local/account/reset/'.$token);
    }

    public function reset(String $token = '')
    {   
        $newPassword = $token;
        $params = [
            'token' => $token,
            'password' => password_hash($newPassword, PASSWORD_BCRYPT),
        ];

        $this->db->query('UPDATE accounts SET password = :password, status = 1, token = "" WHERE token = :token', $params);
        return $newPassword;
    }

    public function save(Array $post = [])
    {   
        $params = [
            'id' => $_SESSION['account']['id'],
            'email' => $post['email'],
            'wallet' => $post['wallet'],
        ];

        if (!empty($post['password'])) {
            $params['password'] = \password_hash($post['password'], PASSWORD_BCRYPT);
            $sql = ' , password = :password';
        } else {
            $sql = '';
        }

        // Обновляем данные в сессии
        foreach ((array)$params as $key => $value) {
            $_SESSION['account'][$key] = $value;
        }

        // Обновляем данные в базе
        // \debug($params);
        $this->db->query('UPDATE accounts SET email = :email, wallet = :wallet'.$sql.' WHERE id = :id', $params);
    }
}

?>