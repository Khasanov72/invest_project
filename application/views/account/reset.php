<div class="container">
    <h3 class="mt-4 mb-3"><?php echo $title ?></h3>
    <p>Новый пароль для входа: <b><?php echo $password ?></b></p>
    <div class="row">
        <a href="/account/login" class="btn btn-primary">Войти</a>
    </div>
</div>